This script is designed to be placed in the folder you installed SmartHunter, so that mhwWrapper.py is in the same folder as SmartHunter.exe.

# Usage

0. *Don't run MHW or SmartHunter before running this script, or they may run twice.*
1. Run mhwWrapper.py
2. Play.  When you exit the game SmartHunter.exe will automatically close.

No really, that's it.

# Requirements

- Python 3.
- psutils for Python 3.  You can install it through cmd/powershell with "python -m pip install psutils".  You may need administrator access.