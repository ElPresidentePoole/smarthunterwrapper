import psutil
import subprocess
import os
import webbrowser
from time import sleep

P_SMART_HUNTER = 'SmartHunter.exe'
P_MH_WORLD = 'MonsterHunterWorld.exe'
URL_MH_WORLD = 'steam://rungameid/582010'

def check_if_process_running(executable):
    try:
        for p in psutil.process_iter():
            if p.name() == executable:
                print('{} found...'.format(executable))
                return True
    except psutil.NoSuchProcess:
        pass
    
    return False

def main():
    smarthunterRunning = False
    mhwRunning = False

    # Wait for SmartHunter.exe
    subprocess.Popen(P_SMART_HUNTER)
    print('Waiting for {}...'.format(P_SMART_HUNTER))
    while not smarthunterRunning:
        smarthunterRunning = check_if_process_running(P_SMART_HUNTER)
    
    # Wait for MonsterHunterWorld.exe
    webbrowser.open(URL_MH_WORLD)
    print('Waiting for {}...'.format(P_MH_WORLD))
    while not mhwRunning:
        mhwRunning = check_if_process_running(P_MH_WORLD)
    
    print('Waiting for {} to close'.format(P_MH_WORLD))
    while mhwRunning:
        sleep(5)
        mhwRunning = check_if_process_running(P_MH_WORLD)
    
    os.system('taskkill /im {}'.format(P_SMART_HUNTER))



if __name__ == '__main__':
    main()
